describe("testing REST API", function() {
    var $httpBackend;

    $httpBackend.when("HEAD", "/api/todos/").respond();
    $httpBackend.when("TRACE", "/api/todos/").respond();
    $httpBackend.when("OPTIONS", "/api/todos/").respond();

});
