# coding: utf-8
from rest_framework import serializers

from .models import TodoObject


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoObject
