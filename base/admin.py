from django.contrib import admin

from .models import TodoObject


admin.site.register(TodoObject)
