# coding: utf-8
from __future__ import unicode_literals
import json

from django.core.urlresolvers import reverse

from django.test import TestCase
from django.test import Client

from .models import TodoObject


class BaseTest(TestCase):
    def _create_object(self, fields=None):
        d = fields or dict(title='123', description='123')
        return TodoObject.objects.create(**d)

    def test_model(self):
        b''' testing model create, delete, update, save '''
        d = dict(title='123', description='123')
        count = TodoObject.objects.count()
        obj = self._create_object(d)
        self.assertEqual(count+1, TodoObject.objects.count())
        self.assertEqual(obj.title, d['title'])
        self.assertEqual(obj.description, d['description'])
        self.assertEqual(obj.is_completed, False)
        obj.is_completed = True
        obj.save()
        self.assertEqual(obj.is_completed, True)
        obj.delete()
        self.assertEqual(count, TodoObject.objects.count())

    def test_api(self):
        b''' testing REST API '''
        self._create_object()
        self._create_object()
        self._create_object()

        client = Client()

        todos_list_url = reverse('todos-list')

        #testing home and list api
        resp = client.get(todos_list_url)
        self.assertEqual(resp.status_code, 200)
        res = json.loads(resp.content)
        self.assertEqual(len(res), 3)

        #testing detail, update object view
        obj = res[0]
        obj_detail_url = reverse('todos-detail', kwargs=dict(pk=obj.get('id')))
        for method in ('get', 'put'):
            func = getattr(client, method)
            if method == 'put':
                resp = func(obj_detail_url, data=json.dumps(obj), content_type="application/json")
            else:
                resp = func(obj_detail_url)
            self.assertEqual(resp.status_code, 200)
            res = json.loads(resp.content)
            self.assertIsNotNone(res.get('title'))
            self.assertIsNotNone(res.get('description'))
            self.assertIsNotNone(res.get('is_completed'))

        # testing delete object view
        resp = client.delete(obj_detail_url)
        self.assertEqual(resp.status_code, 204)
        #testing create object view
        resp = client.post(todos_list_url, data=json.dumps(obj), content_type="application/json")
        self.assertEqual(resp.status_code, 201)
        # check list
        resp = client.get(todos_list_url)
        self.assertEqual(resp.status_code, 200)
        res = json.loads(resp.content)
        self.assertEqual(len(res), 3)

        #testings home and list
        resp = client.get('/')
        self.assertEqual(resp.status_code, 200)
