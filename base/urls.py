from django.conf.urls import include, url

from rest_framework import routers

from . import views


todo_router = routers.DefaultRouter()
todo_router.register(r'todos', views.TodoViewSet, base_name='todos')


urlpatterns = [
    url(r'^$', views.index, name='index'),

    url('^api/', include(todo_router.urls)),
]
