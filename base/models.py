# coding: utf-8
from django.db import models


class TodoObject(models.Model):
    title = models.TextField(max_length=75)
    description = models.TextField()
    is_completed = models.BooleanField(default=False)

    __unicode__ = lambda self: self.title
