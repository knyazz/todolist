# coding: utf-8
from __future__ import unicode_literals

from django.shortcuts import render

from rest_framework import viewsets

from .models import TodoObject
from .serializers import TodoSerializer


class TodoViewSet(viewsets.ModelViewSet):
    b''' TODO list REST API '''
    queryset = TodoObject.objects.all()
    serializer_class = TodoSerializer


# home page
def index(request):
    return render(request, 'base.html')
