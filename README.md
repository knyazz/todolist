INSTALL

activate virtualenv and install requirements.

```
pip install -r python_requirements.txt
./manage.py migrate
./manage.py createsuperuser
./manage.py runserver
```

For tests:
```
py.test --create-db base/test.py
```
