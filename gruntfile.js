module.exports = function(grunt) {
  grunt.initConfig({
    jasmine : {
      //src : 'base/static/js/angular.min.js',
      specs : 'test/*Spec.js'
    }
  });
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.registerTask('default', 'jasmine');
};